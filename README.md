# Trading Statistic Generator Service
Creating JSON formatted daily, weekly, monthly sales and purchases stats 

## requirements
- python 3.8 
- django 3+


## installation
```
pip install -r requirments.txt
```


## Generator Usage
```
python manage.py generator -a
```
```
python manage.py generator --all
```
```
python manage.py generator -ticker BTCTRY
```

## EndPoint
### all stats
```
/
```
### Daily stats
```
/get_stats/daily/
```
### weekly stats
```
/get_stats/weekly/
```
### monthly stats
```
/get_stats/monthly/
```

