from django.http import JsonResponse
from django.shortcuts import render
from django.core.cache import cache


def get_statistics(request, offset=None):
    return JsonResponse(
        cache.get('trading_statistics', {}) if not offset else cache.get('trading_statistics', {}).get(offset))
