from django.db import models


# Create your models here.
class Transaction(models.Model):
    time = models.CharField(max_length=20, primary_key=True, db_index=True)
    amount = models.DecimalField(max_digits=18, decimal_places=8)
    price = models.DecimalField(max_digits=18, decimal_places=8)
    type = models.CharField(max_length=1)
