# !/usr/bin/env python
import json
import time
from datetime import datetime, timedelta
from decimal import Decimal
from statistics import mean
import requests
from django.core.serializers.json import DjangoJSONEncoder
from requests import Timeout
import signal
import sys
import logging

from stats.models import Transaction

log = logging.getLogger('stat_generator')
from threading import Thread

from django.core.management.base import BaseCommand, CommandError
from django.core.cache import cache


class GenerateStatistic:

    def __init__(self, ticker):
        self.orderbook_provider_url = f"https://www.bitexen.com/api/v1/order_book/{ticker}/"
        self.last_transactions = []
        self.transactions = {}
        self.statistic = {}
        self._type = None
        self.offset = None

        qs = Transaction.objects.all()
        self.transactions.update({'B': [{
            'time': item.time,
            'type': item.type,
            'amount': item.amount,
            'price': item.price,
        } for item in qs if item.type == 'B']})
        self.transactions.update({'S': [{
            'time': item.time,
            'type': item.type,
            'amount': item.amount,
            'price': item.price,
        } for item in qs if item.type == 'S']})

    def get_remote_data(self):
        try:
            req = requests.get(self.orderbook_provider_url)
            if req.status_code == 200:
                _json_data = json.loads(req.text)
                if _json_data.get('status') == 'success' and _json_data.get('data'):
                    self.last_transactions = _json_data.get('data').get('last_transactions')
        except Timeout as e:
            log.error(log)

    def db_save(self):
        _purchases = self.transactions.get('B', [])
        _sales = self.transactions.get('S', [])

        _new_purchases = [item for item in self.last_transactions if
                          item['type'] == 'B' and not item['time'] in [p['time'] for p in _purchases]]
        _new_sales = [item for item in self.last_transactions if
                      item['type'] == 'S' and not item['time'] in [p['time'] for p in _sales]]

        if len(_new_purchases) > 0:
            Transaction.objects.bulk_create([Transaction(**item) for item in _new_purchases])
            sys.stdout.write("%s transaction added to db \r" % len(_new_purchases))
            sys.stdout.flush()

        _purchases += _new_purchases
        _sales += _new_sales

        monthly_time_offset = datetime.now() - timedelta(days=28)

        self.transactions.update(
            {'B': [item for item in _purchases if float(item['time']) > monthly_time_offset.timestamp()]})
        self.transactions.update(
            {'S': [item for item in _sales if float(item['time']) > monthly_time_offset.timestamp()]})

    def generate_data(self):
        self.get_remote_data()
        self.db_save()
        daily_time_offset = datetime.now() - timedelta(days=1)
        weekly_time_offset = datetime.now() - timedelta(days=7)
        monthly_time_offset = datetime.now() - timedelta(days=28)
        self.statistic = {
            'daily': {
                'sales': self.generate_stats('S', daily_time_offset),
                'purchases': self.generate_stats('B', daily_time_offset),
            },
            'weekly': {
                'sales': self.generate_stats('S', weekly_time_offset),
                'purchases': self.generate_stats('B', weekly_time_offset)
            },
            'monthly': {
                'sales': self.generate_stats('S', monthly_time_offset),
                'purchases': self.generate_stats('B', monthly_time_offset),
            }
        }
        return self.statistic

    def generate_stats(self, _type, offset):

        self._type = _type
        self.offset = offset

        _transaction = [item for item in self.transactions.get(_type) if float(item['time']) > offset.timestamp()]
        ordered_list = sorted(_transaction, key=lambda i: Decimal(i['price']))
        minimum_price = ordered_list[0].get('price')
        maximum_price = ordered_list[-1].get('price')
        total_volume = sum(Decimal(t['amount']) for t in _transaction)
        average_price = sum(Decimal(t['price']) * Decimal(t['amount']) for t in _transaction) / total_volume
        return {
            'minimum_price': minimum_price,
            'maximum_price': maximum_price,
            'average_price': average_price,
            'total_volume': total_volume,
        }

    def send_to_api(self):
        cache.set('trading_statistics', self.statistic, timeout=None)

    def reponse(self):
        return self.statistic


class Command(BaseCommand):
    help = 'Generate trading statistics by ticker or all'
    available_tickers = ['BTCTRY', 'ETHTRY']
    threads = []

    def starter(self, ticker):
        self.stdout.write(self.style.SUCCESS(f'Start {ticker} statistic generator...'))
        generator = GenerateStatistic(ticker)
        while True:
            generator.generate_data()
            generator.send_to_api()
            r = generator.reponse()
            time.sleep(1)

    def signal_handler(self, sig, frame):
        sys.exit(0)

    def add_arguments(self, parser):
        parser.add_argument('-ticker', nargs='*', type=str, help="specify the ticker you want to create.")
        parser.add_argument('-a', '--all', action='store_true', help="all tickers you want to create.")

    def handle(self, *args, **options):
        if not options['ticker'] and not options['all']:
            self.stdout.write(self.style.ERROR('please choice a ticker all call with -a or --all'))
            self.stdout.write(self.style.WARNING('Ex for all ticker: ./manage.py generator --all '))
            self.stdout.write(self.style.WARNING('Ex for specific ticker: ./manage.py generator -ticker BTCTRY '))
            sys.exit()

        if not options['all']:
            self.available_tickers = options['ticker']

        for ticker in self.available_tickers:
            self.threads.append(Thread(target=self.starter, args=(ticker,), daemon=True))

        for t in self.threads:
            t.start()

        signal.signal(signal.SIGINT, self.signal_handler)
        print('Press Ctrl+C to Stop service')
        signal.pause()
